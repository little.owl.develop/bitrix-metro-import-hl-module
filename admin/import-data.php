<?php

use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

\Bitrix\Main\UI\Extension::load('ui.alerts');

/** @global CMain $APPLICATION */

$rights = $APPLICATION->GetGroupRight('sale');

if ($rights === 'D') {
    $APPLICATION->AuthForm(GetMessage('ACCESS_DENIED'));
}

$haveAccess = $rights === 'W';

Loc::loadMessages(__FILE__);
Loader::includeModule('langame.core');
Loader::includeModule('sale');
Loader::includeModule('highloadblock');

$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

$APPLICATION->SetTitle(Loc::getMessage('LANGAMECORE_IMPORT_DATA_TITLE'));

$formNote = '';
if ($request->isPost() && $request->get('import-metro') && $haveAccess && check_bitrix_sessid()) {
    $urlMetroApi = 'https://api.hh.ru/metro';
    $json = file_get_contents($urlMetroApi);
    $arMetro = json_decode($json, true);
    
    // Получения списка локаций
    $resLocations = \Bitrix\Sale\Location\LocationTable::getList([
        'filter' => [
            '=NAME.LANGUAGE_ID' => LANGUAGE_ID,
            'TYPE_CODE' => ['CITY'],
        ],
        'select' => [
            'ID' => 'ID',
            'PARENT_ID' => 'PARENT_ID',
            'NAME_LANG' => 'NAME.NAME',
            'TYPE_CODE' => 'TYPE.CODE',
        ],
        'order' => [
            'NAME_LANG' => 'ASC',
        ],
    ]);
    
    $arLocations = [];
    while ($arLoc = $resLocations->fetch()) {
        $arLocations[$arLoc['NAME_LANG']] = $arLoc['ID'];
    }
    
    // Функция получения класса сущности
    $fGetEntityDataClass = function ($HlBlockId) {
        if (empty($HlBlockId) || $HlBlockId < 1) {
            return false;
        }
        $hlblock = Bitrix\Highloadblock\HighloadBlockTable::getById($HlBlockId)->fetch();
        $entity = Bitrix\Highloadblock\HighloadBlockTable::git($hlblock);
        $entityDataClass = $entity->getDataClass();
        return $entityDataClass;
    };
    
    // Сущность линий метро
    $metroLinesHlId = \Bitrix\Main\Config\Option::get('langame.core', 'hl_metro_lines');
    // Сущность станций метро
    $metroStationsHlId = \Bitrix\Main\Config\Option::get('langame.core', 'hl_metro_stations');
    
    
    $entityMetroLinesDataClass = $fGetEntityDataClass($metroLinesHlId);
    $entityMetroStationsDataClass = $fGetEntityDataClass($metroStationsHlId);
    
    // Получение массива линий
    $rsData = $entityMetroLinesDataClass::getList([
        'select' => ['*'],
        // 'order' => ['ID' => 'ASC'],
    ]);
    
    $arLines = [];
    while ($el = $rsData->fetch()) {
        // $el['ACTION'] = 'NONE';
        $arLines[$el['UF_NAME']] = $el;
    }
    
    // Получения списка станций
    $rsData = $entityMetroStationsDataClass::getList([
        'select' => ['*'],
        // 'order' => ['ID' => 'ASC'],
    ]);
    
    $arStations = [];
    while ($el = $rsData->fetch()) {
        // $el['ACTION'] = 'NONE';
        $arStations[$el['UF_NAME']] = $el;
    }
    
    // Добавление и обновление данных
    /*
     * Здесь используются запросы к базе данных в цикле, что является ошибкой. Этот момент требует переработки
     */
    // TODO: переделать запросы в цикле на составление группового SQL-запроса
    foreach ($arMetro as $city) {
        $locationId = '';
        if (!isset($arLocations[$city['name']])) continue;
        $locationId = $arLocations[$city['name']];
        
        $sortLines = 0;
        foreach ($city['lines'] as $line) {
            ++$sortLines;
            
            $lineId = '';
            
            // Добавление/обновление линии
            if (isset($arLines[$line['name']])) {
                if (
                    $arLines[$line['name']]['UF_METRO_LINE_COLOR'] != '#' . $line['hex_color']
                    || $arLines[$line['name']]['UF_SORT'] != $sortLines
                ) {
                    // update
                    $result = $entityMetroLinesDataClass::update($arLines[$line['name']]['ID'], [
                        'UF_SORT' => $sortLines,
                        'UF_METRO_LINE_COLOR' => '#' . $line['hex_color'],
                    ]);
                    if ($result->isSuccess()) {
                        $lineId = $arLines[$line['name']]['ID'];
                    }
                }
            } else {
                // add new
                $result = $entityMetroLinesDataClass::add([
                    'UF_NAME' => $line['name'],
                    'UF_SORT' => $sortLines,
                    'UF_METRO_LINE_COLOR' => '#' . $line['hex_color'],
                    'UF_XML_ID' => 'metro_line_' . CUtil::translit($line['name'], 'ru'),
                    'UF_BX_LOCATION_CITY' => $locationId,
                ]);
                if ($result->isSuccess()) {
                    $lineId = $result->getId();
                }
            }
            // Добавление/обновление станции
            if ($lineId) {
                $sortStations = 0;
                foreach ($line['stations'] as $station) {
                    ++$sortStations;
                    if (isset($arStations[$station['name']])) {
                        // Do nothing yet
                    } else {
                        $result = $entityMetroStationsDataClass::add([
                            'UF_NAME' => $station['name'],
                            'UF_SORT' => $sortStations,
                            'UF_METRO_LINE' => $lineId,
                            'UF_XML_ID' => 'metro_station_' . CUtil::translit($station['name'], 'ru'),
                        ]);
                    }
                }
            }
        }
        
    }
    
    $formNote = '<div class="ui-alert ui-alert-success">'
        . Loc::getMessage('LANGAMECORE_IMPORT_DATA_METRO_SUCCESS') . '</div>';
}


require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_after.php';

// Кнопка запроса метро
?>
    <form name="import-metro-form" action="<?=POST_FORM_ACTION_URI?>" method="post" enctype="multipart/form-data">
        <?=$formNote?>
        <?=bitrix_sessid_post()?>
        <input type="hidden" name="import-metro" value="Y" />
        <button class="adm-btn adm-btn-save"
                type="submit"><?=Loc::getMessage('LANGAMECORE_IMPORT_DATA_METRO')?></button>
    </form>
<?php

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin.php';