<?php

use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

$eventManager = \Bitrix\Main\EventManager::getInstance();
$eventManager->addEventHandler('main', 'OnBuildGlobalMenu', 'langameCoreMenuHandler');

function langameCoreMenuHandler(&$globalMenu, &$moduleMenu)
{
    
    $moduleMenu[] = [
        'parent_menu' => 'global_menu_services',
        'section' => 'clubs',
        'sort' => 1250,
        'text' => Loc::getMessage('LANGAMECORE_DELIVERIES_MENU_CLUBS'),
        'title' => Loc::getMessage('LANGAMECORE_DELIVERIES_MENU_CLUBS'),
        'icon' => '',
        'page_icon' => '',
        'items_id' => 'menu_clubs',
        'items' => [
            [
                'text' => Loc::getMessage('LANGAMECORE_DELIVERIES_MENU_CLUBS_IMPORT_DATA'),
                'title' => Loc::getMessage('LANGAMECORE_DELIVERIES_MENU_CLUBS_IMPORT_DATA'),
                'url' => LANGAMECORE_MID . '_' . 'import-data.php' . '?lang=' . LANG,
                'more_url' => [
                ],
            ],
        ],
    ];
}