<?php
use \Bitrix\Main\Application;

/**
 * Путь до модуля
 * @var string
 */
define('LANGAMECORE_MODULE_PATH', __DIR__);
define('LANGAMECORE_RELATIVE_PATH', str_ireplace(Application::getDocumentRoot(),'', LANGAMECORE_MODULE_PATH));
define('LANGAMECORE_MID', 'langame.core');

/**
 * Подключение composer-а
 */
// require_once 'vendor/autoload.php';