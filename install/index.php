<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\ModuleManager;
use Bitrix\Main\Config\Option;
use Bitrix\Main\EventManager;
use Bitrix\Main\Application;
use Bitrix\Main\IO\Directory;
use Bitrix\Main\IO\File;

Loc::loadMessages(__FILE__);

class langame_core extends CModule
{
    
    public function __construct()
    {
        if (is_file(__DIR__ . '/version.php')) {
            include_once(__DIR__ . '/version.php');
            
            /** @var array $arModuleVersion */
            
            $this->MODULE_ID = str_replace('_', '.', get_class($this));
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
            $this->MODULE_NAME = Loc::getMessage('LANGAMECORE_NAME');
            $this->MODULE_DESCRIPTION = Loc::getMessage('LANGAMECORE_DESCRIPTION');
            
            $this->exclusionAdminFiles = [
                '..',
                '.',
                'menu.php',
            ];
        } else {
            CAdminMessage::showMessage(
                Loc::getMessage('LANGAMECORE_FILE_NOT_FOUND') . ' version.php'
            );
        }
    }
    
    public function doInstall()
    {
        
        global $APPLICATION;
        
        // мы используем функционал нового ядра D7 — поддерживает ли его система?
        if (CheckVersion(ModuleManager::getVersion('main'), '14.00.00')) {
            // копируем файлы, необходимые для работы модуля
            $this->installFiles();
            // создаем таблицы БД, необходимые для работы модуля
            $this->installDB();
            // регистрируем модуль в системе
            ModuleManager::registerModule($this->MODULE_ID);
            // регистрируем обработчики событий
            $this->installEvents();
        } else {
            CAdminMessage::showMessage(
                Loc::getMessage('LANGAMECORE_INSTALL_ERROR')
            );
            return;
        }
        
        $APPLICATION->includeAdminFile(
            Loc::getMessage('LANGAMECORE_INSTALL_TITLE') . ' «' . Loc::getMessage('LANGAMECORE_NAME') . '»',
            __DIR__ . '/step.php'
        );
    }
    
    public function installDB()
    {
    
    }
    
    public function installFiles()
    {
        
        $moduleAbsPath = $this->getModulePath();
        $moduleRelativePath = $this->getModulePath(false);
        $bitrixAdminDir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin';
        
        // Если в модуле есть административные страницы
        if (Directory::isDirectoryExists($moduleAbsPath . '/admin')) {
            // Копируем их из специальной директории в install
            CopyDirFiles($moduleAbsPath . '/install/admin/', $bitrixAdminDir);
            
            // Помещать в install не обязательно, папка admin в корне модуля будет просканирована и файлы
            // будут созданы автоматически
            if ($dir = opendir($moduleAbsPath . '/admin')) {
                while (false !== $file = readdir($dir)) {
                    if (in_array($file, $this->exclusionAdminFiles)) {
                        continue;
                    } else {
                        $fileToCreate = $bitrixAdminDir . '/';
                        $fileToCreate .= $this->MODULE_ID . '_' . $file;
                        $require = '<?php require $_SERVER[\'DOCUMENT_ROOT\'] . \'' . $moduleRelativePath . '/admin/' . $file . '\';';
                        file_put_contents($fileToCreate, $require);
                    }
                }
                closedir($dir);
            }
        }
        
        if (Directory::isDirectoryExists($moduleAbsPath . '/install/themes')) {
            CopyDirFiles($moduleAbsPath . "/install/themes/", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/themes", true, true);
        }
    }
    
    public function installEvents()
    {
    
    }
    
    public function doUninstall()
    {
        
        global $APPLICATION;
        
        $this->uninstallFiles();
        $this->uninstallDB();
        $this->uninstallEvents();
        
        ModuleManager::unRegisterModule($this->MODULE_ID);
        
        $APPLICATION->includeAdminFile(
            Loc::getMessage('LANGAMECORE_UNINSTALL_TITLE') . ' «' . Loc::getMessage('LANGAMECORE_NAME') . '»',
            __DIR__ . '/unstep.php'
        );
        
    }
    
    public function uninstallFiles()
    {
        
        $moduleAbsPath = $this->getModulePath();
        $bitrixAdminDir = $_SERVER['DOCUMENT_ROOT'] . '/bitrix/admin';
        
        if (Directory::isDirectoryExists($moduleAbsPath . '/admin')) {
            DeleteDirFiles($moduleAbsPath . '/install/admin/', $bitrixAdminDir);
            
            if ($dir = opendir($moduleAbsPath . '/admin')) {
                while (false !== $file = readdir($dir)) {
                    if (in_array($file, $this->exclusionAdminFiles)) {
                        continue;
                    } else {
                        File::deleteFile($bitrixAdminDir . '/' . $this->MODULE_ID . '_' . $file);
                    }
                }
                closedir($dir);
            }
        }
    }
    
    public function uninstallDB()
    {
        // удаляем настройки нашего модуля
        Option::delete($this->MODULE_ID);
    }
    
    public function uninstallEvents()
    {
    
    }
    
    /**
     * Возвращает путь до модуля
     * @return string - путь до модуля
     */
    private static function getModulePath($absolute = true)
    {
        $path = dirname(__DIR__);
        $path = str_replace('\\', '/', $path);
        
        if ($absolute === true) {
            return $path;
        } else {
            return str_replace(Application::getDocumentRoot(), '', $path);
        }
    }
}