<?php

use Bitrix\Main\Localization\Loc;

/** @global CMain $APPLICATION */

Loc::loadMessages(__FILE__);

if (!check_bitrix_sessid()) {
    return;
}

if ($errorException = $APPLICATION->getException()) {
    // ошибка при установке модуля
    CAdminMessage::showMessage(
        Loc::getMessage('LANGAMECORE_INSTALL_FAILED') . ': ' . $errorException->GetString()
    );
} else {
    // модуль успешно установлен
    CAdminMessage::showNote(
        Loc::getMessage('LANGAMECORE_INSTALL_SUCCESS')
    );
}
?>

<form action="<?=$APPLICATION->getCurPage()?>"> <!-- Кнопка возврата к списку модулей -->
    <input type="hidden" name="lang" value="<?=LANGUAGE_ID?>" />
    <input type="submit" value="<?=Loc::getMessage('LANGAMECORE_RETURN_MODULES')?>" />
</form>