<?php
$MESS['LANGAMECORE_IMPORT_DATA_TITLE'] = 'Импорт данных';

$MESS['LANGAMECORE_IMPORT_DATA_METRO'] = 'Импортировать метро';
$MESS['LANGAMECORE_IMPORT_DATA_METRO_SUCCESS'] = 'Импорт метро прошёл успешно';
$MESS['LANGAMECORE_IMPORT_DATA_METRO_ERROR'] = 'Произошла ошибка при импорте метро';