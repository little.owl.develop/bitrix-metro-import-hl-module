<?php
$MESS['LANGAMECORE_NAME'] = 'Langame Core';
$MESS['LANGAMECORE_DESCRIPTION'] = 'Основные настройки для работы Langame';

$MESS['LANGAMECORE_FILE_NOT_FOUND'] = 'Не найден файл';
$MESS['LANGAMECORE_INSTALL_TITLE'] = 'Установка модуля';
$MESS['LANGAMECORE_INSTALL_ERROR'] = 'Версия главного модуля ниже 14, обновите систему.';
$MESS['LANGAMECORE_INSTALL_SUCCESS'] = 'Модуль успешно установлен';
$MESS['LANGAMECORE_INSTALL_FAILED'] = 'Ошибка при установке модуля';

$MESS['LANGAMECORE_UNINSTALL_TITLE'] = 'Удаление модуля';
$MESS['LANGAMECORE_UNINSTALL_SUCCESS'] = 'Модуль успешно удален';
$MESS['LANGAMECORE_UNINSTALL_FAILED'] = 'Ошибка при удалении модуля';

$MESS['LANGAMECORE_RETURN_MODULES'] = 'Вернуться в список модулей';