<?php

use Bitrix\Main\Localization\Loc;
use Bitrix\Main\HttpApplication;
use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;

/** @global CMain $APPLICATION */

Loc::loadMessages(__FILE__);

// получаем идентификатор модуля
$request = HttpApplication::getInstance()->getContext()->getRequest();
$moduleId = htmlspecialchars($request['mid'] != '' ? $request['mid'] : $request['id']);
// подключаем наш модуль
Loader::includeModule($moduleId);
// остальные модули
Loader::includeModule('highloadblock');

$allHighloadTable = \Bitrix\Highloadblock\HighloadBlockTable::getList([
    'order' => ['ID' => 'ASC'],
])->fetchAll();

$allHighloadTableSelect = [
    '' => Loc::getMessage('LANGAMECORE_OPTIONS_SELECT_NOT_SELECTED'),
];
foreach ($allHighloadTable as $hl) {
    $allHighloadTableSelect[$hl['ID']] = '[' . $hl['ID'] . '] ' . $hl['NAME'];
}

/*
 * Параметры модуля со значениями по умолчанию
 */
$aTabs = [
    [
        /*
         * Вкладка "Общие настройки"
         */
        'DIV' => 'common',
        'TAB' => Loc::getMessage('LANGAMECORE_OPTIONS_TAB_COMMON'),
        'TITLE' => Loc::getMessage('LANGAMECORE_OPTIONS_TAB_COMMON'),
        'OPTIONS' => [
            /*
             * Секция "Связи сущностей"
             */
            Loc::getMessage('LANGAMECORE_OPTIONS_SECTION_ENTITIES_LINKS'),
            [
                'hl_metro_lines',
                Loc::getMessage('LANGAMECORE_OPTIONS_HL_METRO_LINES'),
                '', // default value
                [
                    'selectbox',
                    $allHighloadTableSelect,
                ],
            ],
            [
                'hl_metro_stations',
                Loc::getMessage('LANGAMECORE_OPTIONS_HL_METRO_STATIONS'),
                '', // default value
                [
                    'selectbox',
                    $allHighloadTableSelect,
                ],
            ],
        ],
    ],
];

/*
 * Создаем форму для редактирвания параметров модуля
 */
$tabControl = new CAdminTabControl(
    'tabControl',
    $aTabs
);

$tabControl->begin();
?>
    <form action="<?=$APPLICATION->getCurPage()?>?mid=<?=$moduleId?>&lang=<?=LANGUAGE_ID?>" method="post">
        <?=bitrix_sessid_post();?>
        <?php
        foreach ($aTabs as $aTab) { // цикл по вкладкам
            if ($aTab['OPTIONS']) {
                $tabControl->beginNextTab();
                __AdmSettingsDrawList($moduleId, $aTab['OPTIONS']);
            }
        }
        $tabControl->buttons();
        ?>
        <input type="submit" name="apply"
               value="<?=Loc::GetMessage('LANGAMECORE_OPTIONS_INPUT_APPLY')?>" class="adm-btn-save" />
        <input type="submit" name="default"
               value="<?=Loc::GetMessage('LANGAMECORE_OPTIONS_INPUT_DEFAULT')?>" />
    </form>

<?php
$tabControl->end();

/*
 * Обрабатываем данные после отправки формы
 */
if ($request->isPost() && check_bitrix_sessid()) {
    
    foreach ($aTabs as $aTab) { // цикл по вкладкам
        foreach ($aTab['OPTIONS'] as $arOption) {
            if (!is_array($arOption)) { // если это название секции
                continue;
            }
            if ($arOption['note']) { // если это примечание
                continue;
            }
            if ($request['apply']) { // сохраняем введенные настройки
                $optionValue = $request->getPost($arOption[0]);
                if ($arOption[3][0] == 'checkbox') {
                    if ($optionValue == '') {
                        $optionValue = 'N';
                    }
                }
                Option::set($moduleId, $arOption[0], is_array($optionValue) ? implode(',', $optionValue) : $optionValue);
            } elseif ($request['default']) { // устанавливаем по умолчанию
                Option::set($moduleId, $arOption[0], $arOption[2]);
            }
        }
    }
    
    LocalRedirect($APPLICATION->getCurPage() . '?mid=' . $moduleId . '&lang=' . LANGUAGE_ID);
}